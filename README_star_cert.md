## Generate a wildcard ssl certificate request for a subdomain, e.g, *.foobar.stanford.edu:

For wildcard cert prerequisites, please refer: [ssl-wildcard](https://uit.stanford.edu/service/ssl/wildcard).

```
$ git clone git@code.stanford.edu:devops-tools/ssl-certificate-utils.git
$ cd star-cert-request
$ ./create-star-cert-req.sh -s foobar -o "Your Organization Name" -e "your-contact-email@lists.stanford.edu"
```

The default top domain is *stanford.edu*, which you can change at the top of the script.

**Note:** the subdomain key and the csr will be saved in your current working directory. Make sure the files are protected.

The generated CSR has `CN=foobar.stanford.edu, X509v3 Subject Alternative Name: DNS:foobar.stanford.edu, DNS:*.foobar.stanford.edu`

To make sure everything looks good before you submit the request:

```
$ openssl req -noout -text -in foobar.stanford.edu.csr
```

## Upload cert to AWS IAM service

Make sure `foobar_stanford_edu_cert.cer` and `foo_stanford_edu_interm.cer` are in the current directory. The foobar_stanford_edu_interm.cer should be the reverse 
order root chain file (3rd from certs you download from InCommon) for AWS. 

```
./upload_cert_to_aws.sh -s foobar.stanford.edu -p foobar.stanford.edu
```

If the SSL certificate already exists, the script will rename it and upload the new server certificate with the same server name.
If you use the cert in ELB, the ELB still uses the old certificate until you update the ELB cert to points to the new SSL cert.

