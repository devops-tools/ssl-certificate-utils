#!/bin/sh
fqdn=$1
port=${2:-443}
[ -x $fqdn ] && echo "FQDN of the site is required." && exit 1
#! host $fqdn > /dev/null && echo "$fqdn is not a vaild host." && exit 1
echo "" | openssl s_client -servername $fqdn -connect $fqdn:$port 2>/dev/null | openssl x509 -text
