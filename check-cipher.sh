#!/bin/sh
fqdn=$1
port=${2:-443}
[ -x $fqdn ] && echo "FQDN of the site is required." && exit 1
! host $fqdn > /dev/null && echo "$fqdn is not a vaild host." && exit 1
! which nmap && echo "namp command is required." && exit 1
nmap --script ssl-enum-ciphers -p 443 $fqdn
