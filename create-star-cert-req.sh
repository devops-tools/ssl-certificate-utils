#!/bin/bash -e
#
# Generate star ssl certificate request for a subdomain, e.g, *.anchorage.stanford.edu:
#
# ./create-star-cert-req.sh -s anchorage -o "IT Services" -e "emerging-tech@lists.stanford.edu"
# 
# Note: the subdomain key and the csr will be saved in your current working directory. 
# 
# If the subdomain is delegated to a cloud vendor, you will need to send the csr to its-ssl-service@lists.stanford.edu 
# Otherwise you can use https://tools.stanford.edu/cgi-bin/cert-request form to submit your request. 
# 

# Default top domain
domain="stanford.edu"

while getopts ":s:o:e:" OPTION
do
  case $OPTION in
    d)
      domain=$OPTARG
      ;;
    s)
      # extract subdomain
      subdomain="${OPTARG%%.$domain}"
      ;;
    o)
      ou=$OPTARG
      ;;
    e)
      email=$OPTARG
      ;;
    ?)
      echo "$0 -s <subdomain> -o <ou> -e <email>"
      exit 0
      ;;
  esac
done

if [[ ! $subdomain|| ! $ou || ! $email || ! $domain ]]; then
    echo "subdomain, ou, or email are missing."
    echo "Usage: $0 -s <subdomain> -o <ou> -e <email>"
    exit 1
fi

echo "creating the $subdomain.key and $subdomain.csr...."
cat site.cnf.tmpl | sed "s/FQDN/$subdomain.$domain/;s/OU/$ou/;s/EMAIL/$email/" > $subdomain.cnf
openssl req -new -config $subdomain.cnf -nodes -keyout $subdomain.$domain.key -out $subdomain.$domain.csr

echo "$subdomain.csr is generated:"
openssl  req -text -noout -in $subdomain.$domain.csr

echo "self-signed cert is generated:"
openssl x509 -req -days 3650 -in $subdomain.$domain.csr -signkey $subdomain.$domain.key -out $subdomain.$domain.crt
