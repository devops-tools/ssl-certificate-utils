#!/bin/bash -e
# For AWS ELB, the cert chain file needs to be in reverse order - root cert at the top (the 3rd file)
# Usage: ./upload_cert_to_aws.sh  -p anchorage -s foo.stanford.edu
#
# To get arn: aws --profile awsprofile iam get-server-certificate --server-certificate-name=<fqdn>
#
# Author: Xueshan Feng <sfeng@stanford.edu>
# 

function init(){
    domain=${domain:-'stanford.edu'}
    server="${server%%.*}"
    echo "=== $server"
    domaincert=${body:-'stanford_edu_cert.cer'}
    intermcert=${chain:='stanford_edu_interm.cer'}
    server_name=$server.$domain
}

function upload_cert(){
  if aws --profile $profile iam get-server-certificate --server-certificate-name $server_name >/dev/null 2>&1 ;
  then
    expirationdate=$(aws --profile $profile iam get-server-certificate --server-certificate-name $server.stanford.edu | \
        jq -r '.ServerCertificate.ServerCertificateMetadata.Expiration' | sed 's/T.*//' )
    echo "Renaming old server $server_name to $server_name.$expirationdate"
    answer='N'
    echo -n "Do you want to continue? [Y/N]"
    read answer
    echo ""
    [ "X$answer" != "XY" ] && echo "Do nothing. Quit." &&  exit 0
    aws --profile $profile iam update-server-certificate --server-certificate-name $server_name \
	--new-server-certificate-name $server_name.$expirationdate
  fi
  aws --profile $profile iam upload-server-certificate --server-certificate-name $server_name \
    --certificate-body file://${server}_${domaincert} \
    --private-key file://$server_name.key --certificate-chain file://${server}_$intermcert
}

help(){
  echo "upload_cert_to_aws.sh -p <profile> -s <server> [-d <domain>] [-c <chain file>] [-b <body>]"
  echo ""
  echo " -p <aws profile>: authenticate as this profile."
  echo " -s <server>: server name. e.g. foo.stanford.edu"
  echo " -d <domain>: default to stanford.edu."
  echo " -b <cert file>: default to <server>_stanford_edu_cert.cer in current directory."
  echo " -c <chain file>: default to <server>_stanford_edu_interm.cer in current directory"
  echo " -h     : Help"
}

# Main

while getopts "b:c:d:p:s:h" OPTION
do
  case $OPTION in
    b)
      body=$OPTARG
      ;;
    c)
      chain=$OPTARG
      ;;
    d)
      domain=$OPTARG
      ;;
    p)
      profile=$OPTARG
      ;;
    s)
      server=$OPTARG
      ;;
    [h?])
      help
      exit
      ;;
  esac
done

if [[ -z $profile || -z $server ]]; 
then
  help
  exit 1
else
  init
fi

echo "Getting AWS account number ..."
accountId=$(aws --profile $profile sts get-caller-identity | jq -r ".Account")
if [ -z "$accountId" ]; then
  echo "Cannot find AWS account number."
  exit 1
fi

if [[ -f ${server}_${domaincert} ]] && [[ -f ${server}_$intermcert ]] && [[ -f $server_name.key ]];
then
  upload_cert
else
  echo "one of the files are missing: ${server}_${domaincert}, ${server}_$intermcert, or $server.key" 
  exit 1
fi

exit 0
