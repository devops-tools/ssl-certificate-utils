#!/bin/bash

# Script to verify key, cert, rootCA chain. Generate a pem bundle for haproxy.

AWS_PROFILE=${AWS_PROFILE:-NODEFAULT}
CERT_BUCKET=${CERT_BUCKET:-DODEFAULT}
fqdn=$1
shortname=${fqdn/.stanford.edu/}

# Error checking
[ -z "$fqdn" ] && echo "Usage: ./$(basename $0) <fqdn>" && exit 1

if ! aws --profile ${AWS_PROFILE} sts get-caller-identity > /dev/null ;
then
  echo "Cannot verify ${AWS_PROFILE}. Use export AWS_PROFILE=<myprofile> to set default."
  exit 1 
fi
if ! aws --profile ${AWS_PROFILE} s3 ls ${CERT_BUCKET} > /dev/null ;
then
  echo Cannot verify ${CERT_BUCKET}.
  echo use export CERT_BUCKET=s3://... to set default.
  exit 1 
fi

echo Checking key file $fqdn
[ ! -f $fqdn.key ] && echo "$fqdn.key doesn't exit." && exit 1
keymd5=$(openssl  rsa -noout -modulus -in $fqdn.key | openssl md5)
echo "md5 = $keymd5"

echo Checking server cert $cert
cert="$(echo $fqdn | tr '.' '_')_cert.cer"
[ ! -f $cert ] && echo "$cert doesn't exit." && exit 1
certmd5=$(openssl x509 -noout -modulus -in $cert  | openssl md5)
echo "md5 = $certmd5"

if [ "$keymd5" != "$certmd5" ]
then
  echo "Certificate and private key doesn't match."
  exit 1
fi

echo Checking CA chain
interm="$(echo $fqdn | tr '.' '_')_interm.cer"
[ ! -f $interm ] && echo "$interm doesn't exit." && exit 1
if ! openssl verify -CAfile $interm  $cert |grep OK ;
then
  echo "CA chain verification failed."
  exit 1
fi

echo Check CAfile. Should be in intermidate1, intermiate2,...rootca format.
if ! openssl  x509 -noout -text -in $interm  | grep CN=InCommon ;
then
  echo Haproxy needs CA bundle should be in this order: intermidate1, intermiate2,...rootca. 
else
  echo ""
fi

for i in $cert $interm $fqdn.key
do
  cat $i
  echo ""
done | tr '\r' '\n' > $shortname.pem

echo "$shortname.pem is saved. "
echo "uploading to s3."
aws --profile ${AWS_PROFILE} s3 cp $shortname.pem ${CERT_BUCKET}/$shortname.pem && \
    aws --profile ${AWS_PROFILE} s3 ls ${CERT_BUCKET}/haproxy-dev/
